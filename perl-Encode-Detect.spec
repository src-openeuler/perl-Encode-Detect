Name:          perl-Encode-Detect
Version:       1.01
Release:       30
Summary:       An Encode::Encoding subclass that detects the encoding of data
License:       MPL-1.1 OR GPL-2.0-or-later OR LGPL-2.0-or-later
URL:           https://metacpan.org/release/Encode-Detect
Source0:       https://cpan.metacpan.org/authors/id/J/JG/JGMYERS/Encode-Detect-%{version}.tar.gz
BuildRequires: perl-devel perl-generators perl(ExtUtils::CBuilder)
BuildRequires: perl(Module::Build) perl(Test::More)
Requires:      perl(Encode::Encoding)

%description
This Perl module is an Encode::Encoding subclass that uses Encode::Detect::Detector to determine
the charset of the input data and then decodes it using the encoder of the detected charset.
It does not require the configuration of a set of expected encodings.

%package       help
Summary:       Help for perl-Encode-Detect
BuildArch:     noarch

%description   help
This package contains man manual for perl-Encode-Detect.

%prep
%autosetup  -n Encode-Detect-%{version} -p1

%build
%{__perl} Build.PL installdirs=vendor optimize="${RPM_OPT_FLAGS}"
./Build

%check
./Build test

%install
./Build install destdir="${RPM_BUILD_ROOT}" create_packlist=0
%{_fixperms} "${RPM_BUILD_ROOT}"/*

%files
%license LICENSE
%doc Changes
%{perl_vendorarch}/auto/Encode
%{perl_vendorarch}/Encode

%files help
%{_mandir}/man3/*

%changelog
* Fri Jan 17 2025 Funda Wang <fundawang@yeah.net> - 1.01-30
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Dec 20 2019 Tianfei <tianfei16@huawei.com> - 1.01-29
- Package init

